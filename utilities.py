import os
import numpy as np
from astropy.io import fits
from astropy.table import Table, Column
from sqlalchemy import Column as sqlColumn
from sqlalchemy import ForeignKey, Float, String, Boolean, Integer, BigInteger

# Gros hack importe de :
# https://rehalcon.blogspot.fr/2010/03/sqlalchemy-programmingerror-cant-adapt.html
from psycopg2.extensions import register_adapter, AsIs

def adapt_numpy_float64(numpy_float64):
    return AsIs(numpy_float64)
register_adapter(np.float64, adapt_numpy_float64)
# fin du hack

def adapt_numpy_int64(numpy_int64):
    return AsIs(numpy_int64)
register_adapter(np.int64, adapt_numpy_int64)

def adapt_numpy_float32(numpy_float32):
    return AsIs(numpy_float32)
register_adapter(np.float32, adapt_numpy_float32)


class Utilities(object):
    """ Utilities class """

    @staticmethod
    def show_query(cur, title, qry):
        """
        Show query

        Parameters:
        -----------
        cur : `~psycopg2.connect`
            Cursor
        title : `str`
            Title of the query
        qry : ``
            SQL query
        """
        print('%s' % (title))
        cur.execute(qry)
        for row in cur.fetchall():
            print(row)

    @classmethod
    def create_table_structure_from_fits(cls, Base, fits_file, hdu_name, primary_key=[], foreign_key={}, skip=None):
        """
        Create table architecture
        """
        def merge_classes(Class1, Class2, hdu_name):
            class NewClass(Class1, Class2) :
                __name__ = hdu_name
            return NewClass

        # Prepare table class
        class MyTable(object):
            __tablename__ = hdu_name

        # Read FITS file
        f = fits.open(fits_file)[hdu_name]

        # Loop over column names
        col_name = np.array(f.columns.names)
        col_format = np.array(f.columns.formats)

        for i in range(len(col_name)):
            col_type = None

            if skip is not None:
                if col_name[i] in skip:
                    continue

# FITS format code         Description                     8-bit bytes
#
# L                        logical (Boolean)               1
# X                        bit                             *
# B                        Unsigned byte                   1
# I                        16-bit integer                  2
# J                        32-bit integer                  4
# K                        64-bit integer                  4
# A                        character                       1
# E                        single precision floating point 4
# D                        double precision floating point 8
# C                        single precision complex        8
# M                        double precision complex        16
# P                        array descriptor                8
# Q                        array descriptor                16

            fits2sql = {
                'L': Boolean(),
                # X                        bit                             *
                # B                        Unsigned byte                   1
                'I': Integer(),
                # J                        32-bit integer                  4
                'K': BigInteger(),
                # A                        character                       1
                'E': Float(),
                'D': Float(),
                # C                        single precision complex        8
                # M                        double precision complex        16
                # P                        array descriptor                8
                # Q                        array descriptor                16
            }
            # if col_format[i] == 'E':
            #     col_type = Float()
            # elif col_format[i] == 'D':
            #     col_type = Float()
            # elif col_format[i] == 'K':
            #     col_type = Integer()
            if col_format[i] in fits2sql:
                col_type = fits2sql[col_format[i]]
            elif 'A' in col_format[i]:
                length = col_format[i].replace('A', '')
                col_type = String(length=int(length))
            else:
                print('oops, pas de type?')

            # Fill MyTable class with column properties
            col = sqlColumn(col_name[i], col_type)
            if col_name[i] in primary_key:
                col.primary_key=True
            setattr(MyTable, col_name[i], col)

        # Merge MyTable and Base
        SQLTable = merge_classes(MyTable, Base, hdu_name)

        # Add foreign keys
        cls.add_foreign_key(SQLTable, foreign_key)

        return SQLTable

    @staticmethod
    def fill_table_from_fits(session, SQLTable, fits_file, hdu_name):
        """
        Fill table from a FITS file
        """
        print('Opening FITS file: {}/{}'.format(fits_file, hdu_name))
        f = fits.open(fits_file)[hdu_name]
        data = f.data

        n_rows = len(data)
        col_names = np.array(f.columns.names)
        print('Found colums: {}'.format(col_names))

        for idx in range(n_rows):  # loop on rows

            kwargs = dict()
            for name in col_names:  # loop on columns
                value = data[name][idx]
                if str(value) == 'nan':  #np.isnan(value):
                    value = None
                    print('! NaN value found in col={} (OBS_ID={})'.format(name, data['OBS_ID'][idx]))
                kwargs[name] = value

            #print('Add row: {}'.format(kwargs))
            entry = SQLTable(**kwargs)

            session.add(entry)

        session.commit()

    @staticmethod
    def create_extra_fits_table_from_dl3(release_path, out_dir, out_file):
        """ Create FITS table with provenance informations and additional VO parameters """

        print("Create extra_info files in for {}".format(release_path))
        # Read OBS_INDEX for observations ids
        table_obs = Table.read(release_path + '/' + 'obs-index.fits.gz')

        # READ HDU_INDEX for EVENTS and AREA files
        table_hdu = Table.read(release_path + '/' + 'hdu-index.fits.gz')

        # Outputs to save
        obs_id = list()
        creator = list()  # evts
        obs_mode = list()  # evts
        evt_version = list()  #evts
        emin = list()  # evts
        emax = list()  # evts
        config = list()  # eff. area
        coverage = list()  # moc

        extra = {}
        for obs in table_obs:
            obs_id.append(obs['OBS_ID'])
            print(obs['OBS_ID'])

            row = table_hdu[table_hdu['OBS_ID'] == obs['OBS_ID']]  # all IRFs

            # EVENT
            row_evt = row[row['HDU_TYPE'] == 'events']
            evt_file = release_path + '/' + row_evt['FILE_DIR'][0] + '/' + row_evt['FILE_NAME'][0]
            table_evt = Table.read(evt_file, hdu=row_evt['HDU_NAME'][0])
            creator.append(table_evt.meta['CREATOR'])
            obs_mode.append(table_evt.meta['OBS_MODE'])
            evt_version.append(float(table_evt.meta['EVTVER']))
            emin.append(table_evt['ENERGY'].min())
            emax.append(table_evt['ENERGY'].max())

            # EFFAREA
            row_area = row[row['HDU_TYPE'] == 'aeff']
            area_file = release_path + '/' + row_area['FILE_DIR'][0] + '/' + row_area['FILE_NAME'][0]
            table_area = Table.read(area_file, hdu=row_area['HDU_NAME'][0])
            config.append(table_area.meta['CONFIG'])

            # MOC
            moc_file = release_path + '/' + row_evt['FILE_DIR'][0] + '/' + row_evt['FILE_NAME'][0] + '.moc.txt'
            print(moc_file)
            moc_file = moc_file.replace('/data/', '/moc/')
            print(moc_file)
            with open(moc_file) as f:
	            moc_ascii = f.read()
            print(moc_ascii)
            coverage.append(moc_ascii)


        # Save FITS
        print('{}, {}'.format(obs_id, type(obs_id)))
        out_table = Table()
        out_table.meta['EXTNAME'] = 'EXTRA_INFOS'
        out_table['OBS_ID'] = Column(obs_id, unit='', description='')
        out_table['CREATOR'] = Column(creator, unit='', description='')
        out_table['OBS_MODE'] = Column(obs_mode, unit='', description='')
        out_table['EVTVER'] = Column(evt_version, unit='', description='')
        out_table['EMIN'] = Column(emin, unit='TeV', description='')
        out_table['EMAX'] = Column(emax, unit='TeV', description='')
        out_table['CONFIG'] = Column(config, unit='', description='')
        out_table['COVERAGE'] = Column(coverage, unit='', description='')

        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        out_table.write(out_dir + out_file, format='fits', overwrite=True)


    @staticmethod
    def add_foreign_key(SQLTable, foreign_key):
        # Add foreign keys
        schema = SQLTable.__table_args__['schema']
        for fk_k, fk_v in foreign_key.items():
            SQLTable.__table__.c[fk_k].append_foreign_key(ForeignKey(schema + '.' + fk_v))
