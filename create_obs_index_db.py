#!/usr/bin/env python2

import os
from sqlalchemy import Column, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import create_engine, inspect
from sqlalchemy.orm import sessionmaker

from utilities import Utilities

pg_db = "gavo"
pg_schema = "hess_dr_dachs"

# DB engine
#db_string = "postgresql://postgres:@localhost/hess_data_release"
db_string = "postgresql://ctadesigner:CT4des1gner@localhost/" + pg_db
engine = create_engine(db_string, echo=True)
insp = inspect(engine)

# Create Base table
Base = declarative_base()
# Declare DB schema
setattr(Base, "__table_args__", ({"schema": pg_schema}))

# OBS_INDEX table structure
obs_fits_file = "hess_dl3_dr1/obs-index.fits.gz"
obs_hdu_name = "OBS_INDEX"
OBS_INDEX = Utilities.create_table_structure_from_fits(
    Base,
    fits_file=obs_fits_file,
    hdu_name=obs_hdu_name,
    primary_key=["OBS_ID"]
)

# HDU_INDEX table structure
hdu_fits_file = "hess_dl3_dr1/hdu-index.fits.gz"
hdu_hdu_name = "HDU_INDEX"
HDU_INDEX = Utilities.create_table_structure_from_fits(
    Base,
    hdu_fits_file,
    hdu_hdu_name,
    primary_key=["OBS_ID", "HDU_NAME"],
    foreign_key={"OBS_ID": "OBS_INDEX.OBS_ID"}
)

# EXTRA table structure
dir_extra = "./extra/"
file_extra = "extra_infos.fits"
extra_fits_file = dir_extra + "/" + file_extra
extra_hdu_name = "EXTRA_INFOS"
if not os.path.isfile(extra_fits_file):
    Utilities.create_extra_fits_table_from_dl3(release_path="./hess_dl3_dr1/",
                                               out_dir=dir_extra,
                                               out_file=file_extra)
EXTRA_INFOS = Utilities.create_table_structure_from_fits(
    Base,
    extra_fits_file,
    extra_hdu_name,
    primary_key=["OBS_ID"],
    foreign_key={"OBS_ID": "OBS_INDEX.OBS_ID"}
)


# Create DB structure
view_names = insp.get_view_names(schema=pg_schema)
print(view_names)
if "vo_obscore " in view_names:
    engine.execute("DROP VIEW " + pg_schema + ".vo_obscore;")
Base.metadata.drop_all(engine)
Base.metadata.create_all(engine)

# Begin session
Session = sessionmaker(bind=engine)
session = Session()

# Fill OBS_INDEX table
Utilities.fill_table_from_fits(
    session,
    OBS_INDEX,
    obs_fits_file,
    obs_hdu_name
)

# Fill HDU_INDEX table
Utilities.fill_table_from_fits(
    session,
    HDU_INDEX,
    hdu_fits_file,
    hdu_hdu_name
)

# Fill EXTRA_INFOS table
Utilities.fill_table_from_fits(
    session,
    EXTRA_INFOS,
    extra_fits_file,
    extra_hdu_name
)


# Print id and livetime of all observations
#observations = session.query(OBS_INDEX)
#for obs in observations:
#    print("id: {}, livetime: {} (s)".format(obs.OBS_ID, obs.LIVETIME))

# Print id and livetime for observation 25443
obs = session.query(OBS_INDEX).filter(OBS_INDEX.OBS_ID == 20275).first()
print("id: {}, livetime: {} (s)".format(obs.OBS_ID, obs.LIVETIME))
