SET search_path TO hess_dr_dachs,public;
--SELECT set_sphere_output('DEG');

-- Convert TeV to meters
CREATE OR REPLACE FUNCTION tev2meter(double precision) RETURNS double precision AS $$
  -- (h*c)/[TeV] -> [m]
  SELECT 1.2398419292004204e-18 / $1
$$ LANGUAGE SQL;

-- Convert TSTART/TSTOP to MJD
CREATE OR REPLACE FUNCTION tomjd(double precision) RETURNS double precision AS $$
  -- MJDREFI = 51910
  -- MJDREFF = 7.428703703703703x10-4
  SELECT $1 / 86400
         + 51910
         + 7.428703703703703e-4
$$ LANGUAGE SQL;

-- ObsCore view
-- DROP VIEW hess_dr_dachs.vo_obscore;
CREATE OR REPLACE VIEW hess_dr_dachs.vo_obscore AS
SELECT

-- Create ObsCore fields
    CASE hdu."HDU_TYPE"
        WHEN 'events'::text THEN 'event-list'::text
        ELSE 'cube'::text
    END AS dataproduct_type,
    hdu."HDU_TYPE"::text AS dataproduct_subtype,
    2 AS calib_level,
    'HESS-DR'::text AS obs_collection,
    hdu."OBS_ID"::text AS obs_id,
    'ivo://padc.obspm/hess#'::text || hdu."OBS_ID"::text AS obs_publisher_did,
    'https://hess-dr.obspm.fr/retrieve/'::text || hdu."FILE_NAME"::text AS access_url,
--    text '' AS access_url_local,
    'application/fits'::text AS access_format,
    hdu."SIZE" AS access_estsize,
    obs."OBJECT" AS target_name,
    obs."RA_PNT" AS s_ra,
    obs."DEC_PNT" AS s_dec,
    5::double precision AS s_fov,
    (((('Circle ICRS GEOCENTER '::text || obs."RA_PNT"::text) || ' '::text) || obs."DEC_PNT"::text) || ' '::text) || 5::text AS s_region,
--    cast(scircle(spoint(obs."RA_PNT"*pi()/180.0, obs."DEC_PNT"*pi()/180.0), 5*pi()/180.0) as scircle) AS s_region,
    0.1::double precision AS s_resolution,
    hess_dr_dachs.tomjd(obs."TSTART") AS t_min,
    hess_dr_dachs.tomjd(obs."TSTOP") AS t_max,
    obs."LIVETIME" AS t_exptime,
    0.000001::double precision AS t_resolution,
--    cast(0.1 as double precision) AS s_resolution_min, --optional
--    cast(0.1 as double precision) AS s_resolution_max, --optional
    extra."EMIN" AS em_min_tev,
    extra."EMAX" AS em_max_tev,
    hess_dr_dachs.tev2meter(extra."EMIN") AS em_min,
    hess_dr_dachs.tev2meter(extra."EMAX") AS em_max,
    NULL::double precision AS em_res_power,
    'time;pos;phys.energy'::text AS o_ucd,
    NULL::text AS pol_states,
    'H.E.S.S.'::text AS facility_name,
    'H.E.S.S.'::text AS instrument_name,
    extra."COVERAGE" AS coverage,

-- Include content of hdu-index file
    obs."RA_PNT" as ra_pnt,
    obs."DEC_PNT" as dec_pnt,
    obs."GLON_PNT" as glon_pnt,
    obs."GLAT_PNT" as glat_pnt,
    obs."ZEN_PNT" as zen_pnt,
    obs."ALT_PNT" as alt_pnt,
    obs."AZ_PNT" as az_pnt,
    obs."OBJECT" as object,
    obs."RA_OBJ" as ra_obj,
    obs."DEC_OBJ" as dec_obj,
    obs."OFFSET_OBJ" as offset_obj,
    obs."ONTIME" as ontime,
    obs."LIVETIME" as livetime,
    obs."DEADC" as deadc,
    obs."TSTART" as tstart,
    obs."TSTOP" as tstop,
    obs."DATE-OBS"as date_obs,
    obs."TIME-OBS"as time_obs,
    obs."DATE-END"as date_end,
    obs."TIME-END"as time_end,
    obs."N_TELS" as n_tels,
    obs."TELLIST" as tellist,
    obs."QUALITY" as quality,
    obs."MUONEFF" as muoneff,
    obs."EVENT_COUNT" as event_count,
    obs."TARGET_NAME" as target_name_subset,
    obs."TARGET_TAG" as target_tag,
    obs."TARGET_OFFSET" as target_offset,
    obs."SAFE_ENERGY_LO" as safe_energy_lo,
    obs."SAFE_ENERGY_HI" as safe_energy_hi

FROM
  hess_dr_dachs."HDU_INDEX" hdu
  JOIN hess_dr_dachs."OBS_INDEX" obs ON obs."OBS_ID" = hdu."OBS_ID"
  JOIN hess_dr_dachs."EXTRA_INFOS" extra ON extra."OBS_ID" = obs."OBS_ID"
WHERE
  hdu."HDU_TYPE" = 'events'
;

-- Grant privileges
--GRANT ALL PRIVILEGES ON vo_obscore TO gavo, gavoadmin WITH GRANT OPTION;

-- test query
SELECT * from hess_dr_dachs.vo_obscore;
