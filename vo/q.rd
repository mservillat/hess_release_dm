<?xml version="1.0" encoding="iso-8859-1"?>

<resource schema="hess_dr">
    <meta name="title">H.E.S.S. DL3 public test data release 1</meta>
    <meta name="creationDate">2018-09-14T10:00:00Z</meta>
    <meta name="description" format="plain">The release consists of event lists and instrument response functions for observations of various well-known gamma-ray sources (the Crab nebula, PKS 2155-304, MSH 15-52, RX J1713.7-3946) as well as observations of empty fields for background modeling.</meta>
    <meta name="referenceURL">https://www.mpi-hd.mpg.de/hfm/HESS/pages/dl3-dr1/</meta>
    <meta name="copyright">H.E.S.S. collaboration, 2018.

The data and documentation is publicly released by the H.E.S.S. collaboration
as a contribution to the ongoing efforts to define a common open format for
data level 3 of imaging atmospheric Cherenkov telescopes (IACTs) and IACT
open-source science tool development, to enlarge the community involved in
IACT data analysis.

No scientific publications may be derived from the data. Using the data
for new claims about the astrophysical sources is not permitted.

When using this data, please include the following attribution:

    "This work made use of data from the H.E.S.S. DL3 public test
    data release 1 (HESS DL3 DR1, H.E.S.S. collaboration, 2018)".

Alternatively, use the following shorter version, e.g. for presentations:

    "HESS DL3 DR1, H.E.S.S. collaboration".

These terms of use must be included in all copies in full or part of the data.
    </meta>
    <meta name="creator.name">H.E.S.S. collaboration</meta>
    <meta name="contact.name">Mathieu Servillat</meta>
    <meta name="subject">H.E.S.S.</meta>

    <table id="vo_obscore" onDisk="True" adql="True">
        <meta name="description" format="plain">IVOA ObsCore description of event lists for observations in the
            H.E.S.S. DL3 public test data release 1</meta>
        <meta name="referenceURL">https://www.mpi-hd.mpg.de/hfm/HESS/pages/dl3-dr1/</meta>

        <column name="dataproduct_type" type="text" ucd="meta.id;class"
            description="product type: spectrum or timeseries ..."/>
        <column name="dataproduct_subtype" type="text" ucd="meta.id;class"
            description="product subtype"/>
        <column name="calib_level" type="integer" ucd="meta.id"
            description="calibration level">
            <values nullLiteral="-1"/></column>
        <column name="obs_collection" type="text" ucd="meta.id"
            description="name of the data collection"/>
        <column name="obs_id" type="text" ucd="meta.id"
            description="observation id"/>
        <column name="obs_publisher_did" type="text" ucd="meta.id"
            description="dataset identifier given by the publisher"/>
        <column name="access_url" type="text" ucd="meta.ref.url"
            description="URL used to access dataset"/>
        <column name="access_format" type="text" ucd="meta.id;class"
            description="file content format"/>
        <column name="access_estsize" type="integer" unit="kbyte" ucd="phys.size;meta.file"
            description="estimated size of dataset">
            <values nullLiteral="-1"/></column>
        <column name="target_name" type="text" ucd="meta.id;src"
            description="name of target"/>
        <column name="s_ra" type="real" unit="deg" ucd="pos.eq.ra"
            description="right ascension of target (ICRS)"/>
        <column name="s_dec" type="real" unit="deg" ucd="pos.eq.dec"
            description="declination of target (ICRS)"/>
        <column name="s_fov" type="real" unit="deg" ucd="phys.angSize;obs.field"
            description="average diameter of the covered region"/>
        <column name="s_region" type="text"
            description="region covered as specified in STC or ADQL"/>
        <column name="s_resolution" type="real" unit="arcsec" ucd="pos.angResolution"
            description="spatial resolution of data as FWHM"/>
        <column name="t_min" type="real" unit="d" ucd="time.start;obs.exposure"
            description="start time"/>
        <column name="t_max" type="real" unit="d" ucd="time.end;obs.exposure"
            description="stop time"/>
        <column name="t_exptime" type="real" unit="s" ucd="time.duration;obs.exposure"
            description="total exposure time"/>
        <column name="t_resolution" type="real" unit="s" ucd="time.resolution"
            description="temporal resolution FWHM"/>
        <column name="em_min_tev" type="real" unit="TeV" ucd="em.wl;stat.min"
            description="start in spectral coordinates in TeV"/>
        <column name="em_max_tev" type="real" unit="TeV" ucd="em.wl;stat.max"
            description="stop in spectral coordinates in TeV"/>
        <column name="em_min" type="real" unit="m" ucd="em.wl;stat.min"
            description="start in spectral coordinates"/>
        <column name="em_max" type="real" unit="m" ucd="em.wl;stat.max"
            description="stop in spectral coordinates"/>
        <column name="em_res_power" type="real" ucd="spect.resolution"
            description="spectral resolving power"/>
        <column name="o_ucd" type="text" ucd="meta.ucd"
            description="UCD of observable"/>
        <column name="pol_states" type="text" ucd="phys.polarization"
            description="list of polarization states or NULL if N/A"/>
        <column name="facility_name" type="text" ucd="instr.obsty"
            description="name of facility used for this observation"/>
        <column name="instrument_name" type="text" ucd="meta.id;instr.name"
            description="name of the instrument used for this observation"/>

        <column name="ra_pnt" type="real" unit="deg" ucd="pos.eq.ra"
            description="Observation pointing right ascension"/>
        <column name="dec_pnt" type="real" unit="deg" ucd="pos.eq.dec"
            description="Observation pointing declination"/>
        <column name="glon_pnt" type="real" unit="deg" ucd="pos.gal.lon"
            description="Observation pointing Galactic longitude"/>
        <column name="glat_pnt" type="real" unit="deg" ucd="pos.gal.lat"
            description="Observation pointing Galactic latitude"/>
        <column name="zen_pnt" type="real" unit="deg" ucd="pos"
            description="Observation pointing zenith angle at observation mid-time TMID"/>
        <column name="alt_pnt" type="real" unit="deg" ucd="pos"
            description="Observation pointing altitude at observation mid-time TMID"/>
        <column name="az_pnt" type="real" unit="deg" ucd="pos"
            description="Observation pointing azimuth at observation mid-time TMID"/>
        <column name="object" type="text" ucd="meta.id;src"
            description="Primary target of the observation"/>
        <column name="ra_obj" type="real" unit="deg" ucd="pos.eq.ra"
            description="Right ascension of OBJECT"/>
        <column name="dec_obj" type="real" unit="deg" ucd="pos.eq.dec"
            description="Declination of OBJECT"/>
        <column name="offset_obj" type="real" unit="deg" ucd="instr.offset"
            description="Distance between observation position and OBJECT"/>
        <column name="ontime" type="real" unit="s" ucd="time.duration"
            description="Total observation time (including dead time). Equals TSTOP - TSTART"/>
        <column name="livetime" type="real" unit="s" ucd="time"
            description="Total livetime (observation minus dead time)"/>
        <column name="deadc" type="real" ucd="arith.ratio"
            description="Dead time correction. It is defined such that LIVETIME = DEADC * ONTIME i.e. the fraction of time the telescope was actually able to take data."/>
        <column name="tstart" type="real" ucd="time.start"
            description="Start time of observation relative to the reference time"/>
        <column name="tstop" type="real" ucd="time.end"
            description="End time of observation relative to the reference time"/>
        <column name="date_obs" type="text" ucd="time.start;obs.exposure"
            description="Observation start date"/>
        <column name="time_obs" type="text" ucd="time.start;obs.exposure"
            description="Observation start time"/>
        <column name="date_end" type="text" ucd="time.end;obs.exposure"
            description="Observation end date"/>
        <column name="time_end" type="text" ucd="time.end;obs.exposure"
            description="Observation end time"/>
        <column name="n_tels" type="integer" ucd="meta.number"
            description="Number of participating telescopes">
            <values nullLiteral="-1"/></column>
        <column name="tellist" type="text" ucd="meta.id;instr.tel"
            description="Telescope IDs (e.g. ?1,2,3,4?)"/>
        <column name="quality" type="integer" ucd="meta.code.qual"
            description="Observation data quality. The following levels of data quality are defined: -1 = unknown quality, 0 = best quality, suitable for spectral analysis, 1 = medium quality, suitable for detection, but not spectra (typically if the atmosphere was hazy), 2 = bad quality, usually not to be used for analysis.">
            <values nullLiteral="-2"/></column>
        <column name="muoneff" type="real"
            description="Mean muon efficiency. Currently use definitions from analysis chain, since creating a unified specification is tricky."/>
        <column name="event_count" type="integer" ucd="meta.number"
            description="Number of events">
            <values nullLiteral="-1"/></column>
        <column name="target_name_subset" type="text" ucd="meta.id;src"
            description="The observation subset name"/>
        <column name="target_tag" type="text" ucd="meta.id;src.class"
            description="Target tag"/>
        <column name="target_offset" type="real" unit="deg" ucd="instr.offset"
            description="Distance between observation position and target"/>
        <column name="safe_energy_lo" type="real" unit="TeV" ucd="stat.min;em.energy"
            description="Low energy threshold"/>
        <column name="safe_energy_hi" type="real" unit="TeV" ucd="stat.max;em.energy"
            description="High energy threshold"/>

    </table>

    <data id="import">
        <make table="vo_obscore"/>
    </data>

    <data id="collection" auto="false">
        <register services="__system__/tap#run"/>
        <make table="vo_obscore"/>
    </data>

</resource>
