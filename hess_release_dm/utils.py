import numpy as np
from astropy.coordinates import SkyCoord

def get_observation_list(data_store, **opt):
    """
    Get observation list according to the option

    Parameters
    ----------
    data_store: `~gammapy.data.DataStore`
        IACT data store
    opt: `dict`
        Options are "coord" or "target"
    """

    obs_list = None
    if 'target' in opt.keys():
        target = opt['target']
        idx = np.where(data_store.obs_table['OBJECT'] == target)
        obs_list = data_store.obs_table['OBS_ID'][idx].data
    elif 'coord' in opt.keys():
        coord = opt['coord']
        radius = opt['radius']
        idx = np.where(
            SkyCoord(
                data_store.obs_table['RA_PNT'],
                data_store.obs_table['DEC_PNT'],
                unit='deg',frame='icrs'
            ).separation(coord.transform_to('icrs')).deg < radius.value)[0]
        obs_list = data_store.obs_table['OBS_ID'][idx].data
        
    return obs_list

import yaml

class ConfigHandler(object):
    
    def __init__(self, config_file):
        """
        Class used to get parameters from
        a configuration file using yaml syntax.

        Parameters
        ----------
        config_file : configuration file (yaml format)
        """
        with open(config_file,'r') as stream:
            self.cfg = yaml.load(stream)

    def getValue(self, param1, param2=None, param3=None):
        """
        Function to get value of variable

        Parameters
        ----------
        param1: section's name
        param2: variable's name or subsection name
        param3: variable's name

        Returns
        -------
        Value parameter (string, float...)
        """
        if param3==None and param2==None:
            return self.cfg[param1]
        elif param3==None:
            return self.cfg[param1][param2]
        else:
            return self.cfg[param1][param2][param3]
