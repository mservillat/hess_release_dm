# HESS data release et data model

## Liens utiles
 - https://www.compose.com/articles/using-postgresql-through-sqlalchemy/

## Exemple

### Création de base de données (psycopg2)
```
from make_db import MakeDataBase
db_maker = MakeDataBase(password='dbpass')
db_maker.create('hess_data_release')
db_maker.check('hess_data_release')
```

### Création et manipulation de tables (sqlalchemy)
```
[julien@touques] hess_release_dm $ ipython
In [1]: %run create_obs_index_db.py
```

## Todo
 - conversion format/type FITS->SQL, meilleur moyen ?
 - améliorer la généralisation de de la création de table
 - fermer la session ?
 - introduire le tout dans les scripts existants
 - vérifier qu'on peut faire tout ce qui pouvait être fait avec sqlalchemy (normalement c'est possible)
 - gérer user/password/db de la bonne manière