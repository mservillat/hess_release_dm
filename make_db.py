from psycopg2 import connect
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT

from utilities import Utilities

class HandleDataBase(object):
    """
    Create data base

    Parameters:
    -----------
    db_name : `str`
        Name of the data base
    user : `str`
        Name of the user
    host : `str`
        Name of the host
    password : `str`
        Password
    """
    def __init__(self, db_name='postgres',
                 user='postgres',
                 host='localhost',
                 password=''):
        self.db_name = db_name
        self.user = user
        self.host = host
        self.password = password

        
    def create(self, name):
        """
        Create data base

        Parameters:
        -----------
        name : `str`
            Name of the data base

        Examples:
        ---------
        from make_db import MakeDataBase
        db_maker = MakeDataBase(password='dbpass')
        db_maker.create('hess_data_release')
        """
        try:
            conn = connect(dbname=self.db_name,
                           user=self.user,
                           host=self.host,
                           password=self.password)
        except:
            print "I am unable to connect to the database"

        
        try: 
            conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
            cur = conn.cursor()
            Utilities.show_query(cur, 'current database', 'SELECT current_database()')
            cur.execute('CREATE DATABASE ' + name)
            Utilities.show_query(cur, 'available databases', 'SELECT * FROM pg_database')
            cur.close()
            conn.close()
        except:
            print('data base {} already exists'.format(name))

            
    def check(self, name):
        """
        Create data base

        Parameters:
        -----------
        name : `str`
            Name of the data base

        Examples:
        ---------
        from make_db import MakeDataBase
        db_maker = MakeDataBase(password='dbpass')
        db_maker.check('hess_data_release')
        """
        print('connecting to {} ...'.format(name))
        conn = connect(user=self.user,
                       database=name,
                       host=self.host,
                       password=self.password)
        cur = conn.cursor()
        cur = conn.cursor()
        Utilities.show_query(cur, 'current database', 'SELECT current_database()')
        cur.close()
        conn.close()
