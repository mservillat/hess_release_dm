"""
Make spectra

Parameters:
-----------
configuration file
"""
import sys
import os

import matplotlib.pyplot as plt
import numpy as np
import astropy.units as u
from astropy.coordinates import SkyCoord, Angle
from astropy.table import vstack as vstack_table

from regions import CircleSkyRegion

from gammapy.data import DataStore, ObservationList
from gammapy.data import ObservationStats, ObservationSummary
from gammapy.background.reflected import ReflectedRegionsBackgroundEstimator
from gammapy.utils.energy import EnergyBounds
from gammapy.spectrum import SpectrumExtraction, SpectrumObservation, SpectrumFit, SpectrumResult
from gammapy.spectrum.models import PowerLaw, ExponentialCutoffPowerLaw, LogParabola
from gammapy.spectrum import FluxPoints, SpectrumEnergyGroupMaker, FluxPointEstimator
from gammapy.image import SkyImage

from hess_release_dm.utils import ConfigHandler

# config file
if len(sys.argv) < 2:
    print('param: configuration file')
    sys.exit()

config_file = sys.argv[1]

# open configuration file
try:
    cfg = ConfigHandler(config_file)
except:
    print('File not found: {0}'.format(configfile))
    sys.exit()

# Output directory
output_dir = cfg.getValue('Output','output_dir') + '/'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Save as png
save_as_png = cfg.getValue('Output', 'save_as_png')

# Load all data
release_path = cfg.getValue('General','release_path')
data_store = DataStore.from_dir(release_path)

# List of observations
obs_ids = cfg.getValue('Target','target_obs').split(' ')
obs_ids = [int(i) for i in obs_ids]
obs_list = data_store.obs_list(obs_ids)

# Target position
target_position = SkyCoord(cfg.getValue('Target','target_ra'),
                           cfg.getValue('Target','target_dec'),
                           unit='deg',
                           frame='icrs')
on_region = CircleSkyRegion(center=target_position, radius=0.2 * u.degree)

# Load exclusion mask
EXCLUSION_FILE = '$GAMMAPY_EXTRA/datasets/exclusion_masks/tevcat_exclusion.fits'
allsky_mask = SkyImage.read(EXCLUSION_FILE)
exclusion_mask = allsky_mask.cutout(
    position=on_region.center,
    size=Angle('6 deg'),
)

# Estimate background
background_estimator = ReflectedRegionsBackgroundEstimator(
    obs_list=obs_list,
    on_region=on_region,
    exclusion_mask = exclusion_mask)
background_estimator.run()

# Estimate energy threshold
opt_threshold = dict()
opt_threshold['bin_method'] = cfg.getValue('Analysis','bin_method')
fit_range = None  # Will be filled later for user_defined
print('Fit opts: {}'.format(opt_threshold))
use_recommended_erange = False
if opt_threshold['bin_method'] == 'auto_emin':
    opt_threshold['nbins'] = cfg.getValue('Analysis','safe_nbins')
    opt_threshold['emax'] = cfg.getValue('Analysis','safe_emax') * u.TeV

    thresholds = list()
    for idx in range(len(obs_list)):
        offset = obs_list[idx].pointing_radec.separation(background_estimator.result[idx].on_region.center)
        area = obs_list[idx].aeff.to_effective_area_table(offset=offset,
                                                          energy=np.logspace(-2, 2.5, 109) * u.TeV)
        aeff_thres = 10. / 100. * area.max_area  # 10 percent of max area as it is done above
        eth = area.find_energy(aeff_thres)
        thresholds.append(eth)
    opt_threshold['emin'] = min(thresholds) * u.TeV
    fit_range = [opt_threshold['emin'].value, opt_threshold['emax'].value] * u.TeV
elif opt_threshold['bin_method'] == 'user_defined':
    opt_threshold['nbins'] = cfg.getValue('Analysis','recommanded_nbins')
    opt_threshold['emin'] = cfg.getValue('Analysis','recommanded_emin') * u.TeV
    opt_threshold['emax'] = cfg.getValue('Analysis','recommanded_emax') * u.TeV
    use_recommended_erange = True
else:
    print('Something wrong with bin_method: {}'.format(opt_threshold['bin_method']))
    sys.exit()
    
# Extract data for spectrum
e_reco = np.logspace(np.log10(opt_threshold['emin'].value),
                     np.log10(opt_threshold['emax'].value),
                     opt_threshold['nbins'] + 1).tolist() * u.TeV
extraction = SpectrumExtraction(
    obs_list=obs_list,
    bkg_estimate=background_estimator.result,
    containment_correction=False,
    e_reco=e_reco,
    use_recommended_erange=use_recommended_erange
)
extraction.run()
extraction.compute_energy_threshold(method_lo_threshold='area_max', percent=10.)

model = PowerLaw(
    index=3.5 * u.Unit(''),
    amplitude=1e-12 * u.Unit('cm-2 s-1 TeV-1'),
    reference=1 * u.TeV,
)

# Fit
joint_fit = SpectrumFit(obs_list=extraction.observations,
                        model=model,
                        fit_range=fit_range)
joint_fit.fit()
joint_fit.est_errors()
joint_result = joint_fit.result
print(joint_result[0])

fit_range = joint_fit.result[0].fit_range

# Flux points
stacked_obs = extraction.observations.stack()
seg = SpectrumEnergyGroupMaker(obs=stacked_obs)
if opt_threshold['bin_method'] in 'user_defined':
    # Does not work at all, see Christoph bad code...
    #seg.compute_range_safe()
    fp_ereco = e_reco
    #fp_ereco = fp_ereco[fp_ereco >= fit_range[0] *0.99]
    #fp_ereco = fp_ereco[fp_ereco <= fit_range[-1] *1.01]
    seg.compute_groups_fixed(ebounds=fp_ereco)
else:
    #seg.set_energy_range(e_reco[0], e_reco[-1])
    pass

print(seg.groups)

# Compute flux points
fpe = FluxPointEstimator(
    obs=stacked_obs,
    groups=seg.groups,
    model=joint_result[0].model,
)
fpe.compute_points()

# Final plot
spectrum_result = SpectrumResult(
    points=fpe.flux_points,
    model=joint_result[0].model,
)
ax0, ax1 = spectrum_result.plot(
    energy_range=fit_range,
    fig_kwargs=dict(figsize=(8,8)),
    point_kwargs=dict(color='navy')
)

fpe.flux_points.table.write(output_dir + cfg.getValue('Output', 'spectra_name'),
                            format='fits', overwrite=True)

ax1.set_ylim([-1, 1])
ax1.set_xlim([fit_range[0].value, fit_range[-1].value])

if save_as_png:
    plt.savefig(output_dir + cfg.getValue('Output', 'spectra_preview'), format='png')

if cfg.getValue('Plot', 'show_results') is True:
    plt.show()
