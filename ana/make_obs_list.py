"""
Select list of observations according to
source position or target name

Parameters:
-----------
configuration file
"""

import sys 

import astropy.units as u

from gammapy.data import DataStore
from astropy.coordinates import SkyCoord

from hess_release_dm.utils import get_observation_list, ConfigHandler

# config file
if len(sys.argv) < 2:
    print('param: configuration file')
    sys.exit()

config_file = sys.argv[1]

# open configuration file
try:
    cfg = ConfigHandler(config_file)
except:
    print('File not found: {0}'.format(configfile))
    sys.exit()

# Load all data
release_path = cfg.getValue('General','release_path')
data_store = DataStore.from_dir(release_path)

# Selection of observations
sel_type = cfg.getValue('Selection','sel_type')
opt = dict()

if sel_type == 'target':
    opt['target'] = cfg.getValue('Selection','target_name')
elif sel_type == 'coordinates':
    source_pos = SkyCoord(cfg.getValue('Selection','target_ra'),
                          cfg.getValue('Selection','target_dec'),
                          unit='deg',
                          frame='icrs')
    radius = cfg.getValue('Selection','target_radius')
    opt = dict(coord=source_pos, radius=radius * u. deg)

print('Options for observations selection:')
for key, val in opt.iteritems():
    print('{}: {}'.format(key, val))
    
obs_ids = get_observation_list(data_store, **opt)
print('List: {}'.format(obs_ids))

output_list = cfg.getValue('General','output_list')
f = open(output_list, 'w')
for item in obs_ids:
  f.write("%s\n" % item)
f.close()
