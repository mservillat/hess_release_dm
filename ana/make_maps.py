"""
Make counts, background and significance images

Parameters:
-----------
configuration file
"""

import sys
import os

import numpy as np
import matplotlib.pyplot as plt

import astropy.units as u
from astropy.coordinates import SkyCoord, Angle
from astropy.table import Table
from astropy.visualization import simple_norm
from astropy.convolution import Ring2DKernel, Tophat2DKernel

from gammapy.data import DataStore
from gammapy.image import SkyImage, SkyImageList
from gammapy.utils.energy import Energy
from gammapy.detect import KernelBackgroundEstimator as KBE

from hess_release_dm.utils import ConfigHandler

# config file
if len(sys.argv) < 2:
    print('param: configuration file')
    sys.exit()

config_file = sys.argv[1]

# open configuration file
try:
    cfg = ConfigHandler(config_file)
except:
    print('File not found: {0}'.format(configfile))
    sys.exit()

# Load all data
release_path = cfg.getValue('General','release_path')
data_store = DataStore.from_dir(release_path)

# List of observations
try:
    obs_ids = cfg.getValue('Target','target_obs').split(' ')
except:
    obs_ids = [cfg.getValue('Target','target_obs')]
    raise
    
obs_ids = [int(i) for i in obs_ids]

# Target position
source_pos = SkyCoord(cfg.getValue('Target','target_ra'),
                      cfg.getValue('Target','target_dec'),
                      unit='deg',
                      frame='icrs')

# Define sky image
ref_image = SkyImage.empty(
    nxpix=cfg.getValue('ImageDef','nxpix'),
    nypix=cfg.getValue('ImageDef','nypix'),
    binsz=cfg.getValue('ImageDef','binsz'),
    xref=source_pos.ra.deg, yref=source_pos.dec.deg,
    coordsys='CEL', proj='TAN',
)

# Output directory
output_dir = cfg.getValue('Output','output_dir') + '/'
if not os.path.exists(output_dir):
    os.makedirs(output_dir)

# Save as png
save_as_png = cfg.getValue('Output', 'save_as_png')
    
# Counts map
#ax = plt.gca()
plt.figure('Counts map')
counts_image = SkyImage.empty_like(ref_image)
for obs_id in obs_ids:
    events = data_store.obs(obs_id=obs_id).events
    counts_image.fill_events(events)

norm = simple_norm(counts_image.data, stretch='sqrt', min_cut=0, max_cut=counts_image.data.max())
counts_image = counts_image.smooth(radius=0.1 * u.deg)
counts_image.plot(norm=norm, add_cbar=True)
counts_image.write(filename = output_dir + cfg.getValue('Output', 'counts_name'), overwrite=True)
if save_as_png:
    plt.savefig(output_dir + cfg.getValue('Output', 'counts_preview'), format='png')

# Background image
images = SkyImageList()
images['counts'] = counts_image

source_kernel = Tophat2DKernel(radius=5)
source_kernel.normalize(mode='peak')
source_kernel = source_kernel.array

background_kernel = Ring2DKernel(radius_in=20, width=10)
background_kernel.normalize(mode='peak')
background_kernel = background_kernel.array

kbe = KBE(
    kernel_src=source_kernel,
    kernel_bkg=background_kernel,
    significance_threshold=5,
    mask_dilation_radius=0.06 * u.deg,
)
result = kbe.run(images)

plt.figure('Background map')
background_image = result['background']
norm = simple_norm(background_image.data, stretch='sqrt', min_cut=0, max_cut=background_image.data.max())
background_image.plot(norm=norm, add_cbar=True)
background_image.write(filename = output_dir + cfg.getValue('Output', 'bkg_name'), overwrite=True)
if save_as_png:
    plt.savefig(output_dir + cfg.getValue('Output', 'bkg_preview'), format='png')


plt.figure('Significance map')
significance_image = result['significance']
idx = np.where(np.isfinite(significance_image.data) == True)
max_signifiance = significance_image.data[idx].max()
significance_image.plot(add_cbar=True, vmin=-3, vmax=max_signifiance)
significance_image.write(filename = output_dir + cfg.getValue('Output', 'sig_name'), overwrite=True)
if save_as_png:
    plt.savefig(output_dir + cfg.getValue('Output', 'sig_preview'), format='png')

if cfg.getValue('Plot', 'show_results') is True:
    plt.show()


