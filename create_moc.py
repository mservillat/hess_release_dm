import os
from mocpy import MOC
from astropy import units as u
from astropy.coordinates import SkyCoord, Angle, angular_separation
from astropy.io import fits
from astropy.table import Table

# A function creating all the matplotlib context for plotting a MOC with its perimeter
def plot(moc, title=""):
    import matplotlib.pyplot as plt
    from mocpy import World2ScreenMPL

    fig = plt.figure(figsize=(15, 10))

    with World2ScreenMPL(
        fig,
        fov=330 * u.deg,
        center=SkyCoord(0, 0, unit="deg", frame="galactic"),
        coordsys="galactic",
        rotation=Angle(0, u.degree),
        projection="AIT",
    ) as wcs:
        ax = fig.add_subplot(1, 1, 1, projection=wcs)

        moc.fill(
            ax=ax,
            wcs=wcs,
            edgecolor="r",
            facecolor="r",
            linewidth=1.0,
            fill=True,
            alpha=0.5,
        )
        moc.border(ax=ax, wcs=wcs, color="black", alpha=0.5)

    plt.xlabel("ra")
    plt.ylabel("dec")
    if title:
        plt.title(title)
    plt.grid(color="black", linestyle="dotted")
    plt.show()
    plt.close()


# Get fits files
fits_path = "hess_dl3_dr1/data"
flist = []
for (root, dirs, file) in os.walk(fits_path):
    for f in file:
        if '.fits' in f:
            flist.append(str(f))

# Prepare moc dir
moc_path = "hess_dl3_dr1/moc"
if not os.path.isdir(moc_path):
    os.mkdir(moc_path)

full_moc = MOC()
for filename in flist:

    # Get events
    #filename = "hess_dl3_dr1/data/hess_dl3_dr1_obs_id_020136.fits.gz"
    hdul = fits.open(os.path.join(fits_path, filename))
    table = Table(hdul[1].data)
    header = hdul[1].header
    hdul.close()
    #print(table)

    # remove events with offset larger than 6°
    #sep = angular_separation(header["DEC_PNT"], header["RA_PNT"], table["DEC"], table["RA"])
    cref = SkyCoord(header["RA_PNT"] * u.deg, header["DEC_PNT"] * u.deg, frame='fk5')
    coords = SkyCoord(table["RA"] * u.deg, table["DEC"] * u.deg, frame='fk5')
    sep = cref.separation(coords)
    table_filt = table[sep < 6 * u.deg]

    fits_moc = MOC.from_lonlat(table_filt["RA"].T * u.deg, table_filt["DEC"].T * u.deg, 6)
    #plot(moc=moc, title="EVENTS")
    full_moc = full_moc.union(fits_moc)
    moc_filename = os.path.join(moc_path, filename + ".moc")
    fits_moc.save(moc_filename + ".fits", "fits", overwrite=True)
    fits_moc.save(moc_filename + ".txt", "ascii", overwrite=True)

full_moc.save(os.path.join(moc_path, "hess_dl3_dr1_moc.fits"), "fits", overwrite=True)
plot(moc=full_moc, title="H.E.S.S. DL3 DR1")
